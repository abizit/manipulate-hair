<?php
 defined('_JEXEC') or die; $title = JText::_('COM_MEDIA_CREATE_NEW_FOLDER'); ?>
<button data-toggle="collapse" data-target="#collapseFolder" class="toolbar">
	<span class="icon-folder" title="<?php echo $title; ?>"></span> <?php echo $title; ?>
</button>
