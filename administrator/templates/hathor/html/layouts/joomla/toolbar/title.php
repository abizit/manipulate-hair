<?php
 defined('_JEXEC') or die; $class = 'pagetitle'; if (!empty($displayData['icon'])) { $icons = explode(' ', $displayData['icon']); foreach ($icons as $i => $icon) { $icons[$i] = 'icon-48-' . preg_replace('#\.[^.]*$#', '', $icon); } $class .= ' ' . htmlspecialchars(implode(' ', $icons)); } ?>
<div class="<?php echo $class; ?>">
	<h2>
		<?php echo $displayData['title']; ?>
	</h2>
</div>
