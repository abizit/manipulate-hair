<?php
 defined('_JEXEC') or die; $lang = JFactory::getLanguage(); $lang->load('tpl_hathor', JPATH_ADMINISTRATOR) || $lang->load('tpl_hathor', JPATH_ADMINISTRATOR . '/templates/hathor/language'); $app = JFactory::getApplication(); $doc = JFactory::getDocument(); JHtml::_('bootstrap.loadCss', false, $this->direction); $doc->addStyleSheet('templates/system/css/system.css'); $doc->addStyleSheet('templates/' . $this->template . '/css/template.css'); if (!$this->params->get('colourChoice')) { $colour = 'standard'; } else { $colour = htmlspecialchars($this->params->get('colourChoice')); } $doc->addStyleSheet('templates/' . $this->template . '/css/colour_' . $colour . '.css'); $file = 'language/' . $lang->getTag() . '/' . $lang->getTag() . '.css'; if (is_file($file)) { $doc->addStyleSheet($file); } if ($this->direction == 'rtl') { $doc->addStyleSheet('templates/' . $this->template . '/css/template_rtl.css'); $doc->addStyleSheet('templates/' . $this->template . '/css/colour_' . $colour . '_rtl.css'); } $file = 'language/' . $lang->getTag() . '/' . $lang->getTag().'.css'; if (JFile::exists($file)) { $doc->addStyleSheet($file); } if ($this->params->get('boldText')) { $doc->addStyleSheet('templates/' . $this->template . '/css/boldtext.css'); } $doc->addScript('templates/' . $this->template . '/js/template.js', 'text/javascript'); if ($this->params->get('logoFile')) { $logo = JUri::root() . $this->params->get('logoFile'); } else { $logo = $this->baseurl . '/templates/' . $this->template . '/images/logo.png'; } ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
<jdoc:include type="head" />
<!--[if lt IE 9]>
	<script src="../media/jui/js/html5.js"></script>
<![endif]-->
</head>
<body class="contentpane">
	<jdoc:include type="message" />
	<jdoc:include type="component" />
</body>
</html>
