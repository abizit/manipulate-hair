<?php
 defined('JPATH_PLATFORM') or die; abstract class JHtmlTextdiff { protected static $loaded = array(); public static function textdiff($containerId) { if (isset(static::$loaded[__METHOD__])) { return; } JHtml::_('bootstrap.framework'); JHtml::_('script', 'com_contenthistory/diff_match_patch.js', false, true); JHtml::_('script', 'com_contenthistory/jquery.pretty-text-diff.min.js', false, true); JHtml::_('stylesheet', 'com_contenthistory/jquery.pretty-text-diff.css', false, true, false); JFactory::getDocument()->addScriptDeclaration("
			(function ($){
				$(document).ready(function (){
 					$('#" . $containerId . " tr').prettyTextDiff();
 				});
			})(jQuery);
			" ); static::$loaded[__METHOD__] = true; return; } } 