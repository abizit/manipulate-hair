<?php
 defined('_JEXEC') or die; $script = 'function insertPagebreak() {'."\n\t"; $script .= 'var title = document.getElementById("title").value;'."\n\t"; $script .= 'if (title != \'\') {'."\n\t\t"; $script .= 'title = "title=\""+title+"\" ";'."\n\t"; $script .= '}'."\n\t"; $script .= 'var alt = document.getElementById("alt").value;'."\n\t"; $script .= 'if (alt != \'\') {'."\n\t\t"; $script .= 'alt = "alt=\""+alt+"\" ";'."\n\t"; $script .= '}'."\n\t"; $script .= 'var tag = "<hr class=\"system-pagebreak\" "+title+" "+alt+"/>";'."\n\t"; $script .= 'window.parent.jInsertEditorText(tag, \''.$this->eName.'\');'."\n\t"; $script .= 'window.parent.SqueezeBox.close();'."\n\t"; $script .= 'return false;'."\n"; $script .= '}'."\n"; JFactory::getDocument()->addScriptDeclaration($script); ?>
	<form>
		<table>
			<tr>
				<td class="key" align="right">
					<label for="title">
						<?php echo JText::_('COM_CONTENT_PAGEBREAK_TITLE'); ?>
					</label>
				</td>
				<td>
					<input type="text" id="title" name="title" />
				</td>
			</tr>
			<tr>
				<td class="key" align="right">
					<label for="alias">
						<?php echo JText::_('COM_CONTENT_PAGEBREAK_TOC'); ?>
					</label>
				</td>
				<td>
					<input type="text" id="alt" name="alt" />
				</td>
			</tr>
		</table>
	</form>
	<button onclick="insertPagebreak();"><?php echo JText::_('COM_CONTENT_PAGEBREAK_INSERT_BUTTON'); ?></button>
