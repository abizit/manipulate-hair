<?php
 defined('_JEXEC') or die; $state = $this->get('State'); $message1 = $state->get('message'); $message2 = $state->get('extension_message'); ?>
<table class="adminform">
	<tbody>
		<?php if ($message1) : ?>
		<tr>
			<th><?php echo $message1 ?></th>
		</tr>
		<?php endif; ?>
		<?php if ($message2) : ?>
		<tr>
			<td><?php echo $message2; ?></td>
		</tr>
		<?php endif; ?>
	</tbody>
</table>
