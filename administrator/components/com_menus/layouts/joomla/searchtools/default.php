<?php
 defined('JPATH_BASE') or die; $data = $displayData; $data['options'] = !empty($data['options']) ? $data['options'] : array(); $doc = JFactory::getDocument(); $doc->addStyleDeclaration("
	/* Fixed filter field in search bar */
	.js-stools .js-stools-menutype {
		float: left;
		margin-right: 10px;
	}
	html[dir=rtl] .js-stools .js-stools-menutype {
		float: right;
		margin-left: 10px
		margin-right: 0;
	}
	.js-stools .js-stools-container-bar .js-stools-field-filter .chzn-container {
		padding: 3px 0;
	}
"); unset($data['view']->activeFilters['menutype']); echo JLayoutHelper::render('joomla.searchtools.default', $data, null, array('component' => 'none')); 