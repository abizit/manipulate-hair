<?php
 defined('JPATH_BASE') or die; $data = $displayData; $menuTypeField = $data['view']->filterForm->getField('menutype'); ?>
<div class="js-stools-field-filter js-stools-menutype hidden-phone hidden-tablet">
	<?php echo $menuTypeField->input; ?>
</div>
<?php
echo JLayoutHelper::render('joomla.searchtools.default.bar', $data, null, array('component' => 'none'));