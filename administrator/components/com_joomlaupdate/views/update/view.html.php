<?php
 defined('_JEXEC') or die; class JoomlaupdateViewUpdate extends JViewLegacy { public function display($tpl=null) { $password = JFactory::getApplication()->getUserState('com_joomlaupdate.password', null); $filesize = JFactory::getApplication()->getUserState('com_joomlaupdate.filesize', null); $ajaxUrl = JUri::base().'components/com_joomlaupdate/restore.php'; $returnUrl = 'index.php?option=com_joomlaupdate&task=update.finalise'; JToolbarHelper::title(JText::_('COM_JOOMLAUPDATE_OVERVIEW'), 'arrow-up-2 install'); JToolBarHelper::divider(); JToolBarHelper::help('JHELP_COMPONENTS_JOOMLA_UPDATE'); if (JFactory::getUser()->authorise('core.admin', 'com_joomlaupdate')) { JToolbarHelper::preferences('com_joomlaupdate'); } JHtml::_('behavior.framework', true); JHtml::_('jquery.framework'); $updateScript = <<<ENDSCRIPT
var joomlaupdate_password = '$password';
var joomlaupdate_totalsize = '$filesize';
var joomlaupdate_ajax_url = '$ajaxUrl';
var joomlaupdate_return_url = '$returnUrl';

ENDSCRIPT;
$document = JFactory::getDocument(); $document->addScript('../media/com_joomlaupdate/json2.js'); $document->addScript('../media/com_joomlaupdate/encryption.js'); $document->addScript('../media/com_joomlaupdate/update.js'); JHtml::_('jquery.framework'); JHtml::_('script', 'system/progressbar.js', true, true); JHtml::_('stylesheet', 'media/mediamanager.css', array(), true); $document->addScriptDeclaration($updateScript); parent::display($tpl); } } 