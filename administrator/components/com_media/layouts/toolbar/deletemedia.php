<?php
 defined('_JEXEC') or die; $title = JText::_('JTOOLBAR_DELETE'); ?>
<button onclick="MediaManager.submit('folder.delete')" class="btn btn-small">
	<i class="icon-remove" title="<?php echo $title; ?>"></i> <?php echo $title; ?>
</button>
