jQuery(function ($) {
    $('.homeSlider').bxSlider({
        'autoStart': true, 'auto': true, 'pager':false
    });
    $('.testimonial').bxSlider({
        'autoStart': true, 'auto': true, 'pager': true, 'controls': false
    });
});
