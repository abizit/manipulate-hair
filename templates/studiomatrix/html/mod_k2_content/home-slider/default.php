<?php defined ( '_JEXEC' ) or die ?>

<div class="sliderDiv">
    <ul class="homeSlider" style="list-style: none;">
        <?php foreach ( $items as $key => $item ): ?>
            <?php $thumbpath = $item->image;
            $reg             = pathinfo ( $thumbpath );
            $filename        = substr ( $reg[ 'filename' ] , 0 , - strlen ( '_XL' ) );
            $fullimage = "media/k2/items/src/" . $filename . ".jpg"
            ?>
            <li>
                <img src="<?=$fullimage?>" alt=""/>
                <div class="inner-width">
                    <?php echo $item->introtext; ?>
                    <a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
                        <?php echo JText::_ ( 'K2_READ_MORE' ); ?>
                    </a>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

