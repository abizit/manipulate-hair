<?php
    // no direct access
    defined ( '_JEXEC' ) or die;
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" style="background-image: url('<?= JUri::base().'images/trending_background.jpg' ?>'); height: 590px;">

    <h1>TRENDING</h1>

    <?php if ( count ( $items ) ): ?>
        <?php $item = $items[ 0 ] ?>
        <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
        <?php echo $item->introtext; ?>
        <a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
            <?php echo JText::_ ( 'K2_READ_MORE' ); ?>
        </a>
    <?php endif; ?>
</div>