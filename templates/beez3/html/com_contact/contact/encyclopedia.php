<?php
 defined('_JEXEC') or die; $cparams = JComponentHelper::getParams('com_media'); ?>
<div class="contact<?php echo $this->pageclass_sfx?>">
		<?php $contactLink = ContactHelperRoute::getCategoryRoute($this->contact->catid);?>
		<h3>
			<span class="contact-category"><a href="<?php echo $contactLink; ?>">
				<?php echo $this->escape($this->contact->category_title); ?></a>
			</span>
		</h3>
	<?php if ($this->contact->name && $this->params->get('show_name')) : ?>
		<h2>
			<span class="contact-name"><?php echo $this->contact->name; ?></span>
		</h2>
	<?php endif; ?>

	<div class="encyclopedia_col1">
		<?php if ($this->contact->image ) : ?>
			<div class="contact-image">
			<?php  ?>
			<?php if ($this->contact->address) { JHtml::_('behavior.caption'); }?>
				<?php echo JHtml::_('image', $this->contact->image, JText::_('COM_CONTACT_IMAGE_DETAILS'), array('align' => 'middle', 'class' => 'caption', 'title' => $this->contact->address)); ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="encyclopedia_col2">
		<?php ?>
				<div class="contact-miscinfo">

						<div class="contact-misc">
							<?php echo $this->contact->misc; ?>
						</div>
					</div>


		<?php ?>
		<?php if ($this->contact->con_position && $this->params->get('show_position')) : ?>
			<p class="contact-position"><?php echo $this->contact->con_position; ?></p>
		<?php endif; ?>
		<?php ?>
		<?php if ($this->contact->state && $this->params->get('show_state')) : ?>
			<p class="contact-state"><?php echo $this->contact->state; ?></p>
		<?php endif; ?>
		<?php ?>
		<?php if ($this->contact->country && $this->params->get('show_country')) : ?>
			<p class="contact-country"><?php echo $this->contact->country; ?></p>
		<?php endif; ?>
	</div>

<div class="clr"> </div>
	<?php  if ($this->params->get('presentation_style') != 'plain'):?>
		<?php  echo JHtml::_($this->params->get('presentation_style').'.start', 'contact-slider'); ?>
	<?php endif ?>
<div class="encyclopedia_links">
<?php echo $this->loadTemplate('links'); ?>

</div>
	<?php if ($this->params->get('presentation_style') != 'plain'):?>
			<?php echo JHtml::_($this->params->get('presentation_style').'.end'); ?>
			<?php endif; ?>
</div>
