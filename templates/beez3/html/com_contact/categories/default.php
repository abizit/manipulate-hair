<?php
 defined('_JEXEC') or die; JHtml::addIncludePath(JPATH_COMPONENT.'/helpers'); JHtml::_('behavior.caption'); ?>
<div class="categories-list<?php echo $this->pageclass_sfx;?>">
<?php if ($this->params->get('show_page_heading')) : ?>
<h1>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
</h1>
<?php endif; ?>
	<?php if ($this->params->get('show_base_description')) : ?>
	<?php  ?>
		<?php if ($this->params->get('categories_description')) : ?>
		<div class="category-desc base-desc">
			<?php echo JHtml::_('content.prepare', $this->params->get('categories_description'), '', 'com_contact.categories'); ?>
			</div>
		<?php  else: ?>
			<?php ?>
			<?php  if ($this->parent->description) : ?>
				<div class="category-desc base-desc">
					<?php  echo JHtml::_('content.prepare', $this->parent->description, '', 'com_contact.categories'); ?>
				</div>
			<?php  endif; ?>
		<?php  endif; ?>
	<?php endif; ?>
<?php
echo $this->loadTemplate('items'); ?>
</div>
