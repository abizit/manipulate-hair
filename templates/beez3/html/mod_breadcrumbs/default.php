<?php
 defined('_JEXEC') or die; ?>

<div class = "breadcrumbs<?php echo $moduleclass_sfx; ?>">
<?php if ($params->get('showHere', 1)) { echo '<span class="showHere">' .JText::_('MOD_BREADCRUMBS_HERE').'</span>'; } for ($i = 0; $i < $count; $i++) { if ($i == 1 && !empty($list[$i]->link) && !empty($list[$i - 1]->link) && $list[$i]->link == $list[$i - 1]->link) { unset($list[$i]); } } end($list); $last_item_key = key($list); prev($list); $penult_item_key = key($list); foreach ($list as $key => $item) : $show_last = $params->get('showLast', 1); if ($key != $last_item_key) { if (!empty($item->link)) { echo '<a href="' . $item->link . '" class="pathway">' . $item->name . '</a>'; } else { echo '<span>' . $item->name . '</span>'; } if (($key != $penult_item_key) || $show_last) { echo ' '.$separator.' '; } } elseif ($show_last) { echo '<span>' . $item->name . '</span>'; } endforeach; ?>
</div>
