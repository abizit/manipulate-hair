<?php
 defined('_JEXEC') or die; JHtml::_('formbehavior.chosen', 'select'); ?>
<?php
 $fieldSets = $this->form->getFieldsets('params'); ?>

<legend><?php echo JText::_('COM_CONFIG_TEMPLATE_SETTINGS'); ?></legend>

<?php
 if (!empty($fieldSets['com_config'])):?>

	<fieldset class="form-horizontal">

<?php foreach ($this->form->getFieldset('com_config') as $field) : ?>
			<div class="control-group">
				<div class="control-label">
					<?php echo $field->label; ?>
				</div>
				<div class="controls">
					<?php echo $field->input; ?>
				</div>
			</div>

<?php endforeach; ?>

	</fieldset>

<?php else: foreach ($fieldSets as $name => $fieldSet) : $label = !empty($fieldSet->label) ? $fieldSet->label : 'COM_CONFIG_' . $name . '_FIELDSET_LABEL'; if (isset($fieldSet->description) && trim($fieldSet->description)) : echo '<p class="tip">' . $this->escape(JText::_($fieldSet->description)) . '</p>'; endif; ?>

<fieldset class="form-horizontal">

	<?php foreach ($this->form->getFieldset($name) as $field) : ?>
		<div class="control-group">
			<div class="control-label">
				<?php echo $field->label; ?>
			</div>
			<div class="controls">
				<?php echo $field->input; ?>
			</div>
		</div>
	<?php endforeach; ?>
</fieldset>
	<?php endforeach; endif; 