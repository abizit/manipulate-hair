<?php
 defined('_JEXEC') or die; class PlgButtonArticle extends JPlugin { protected $autoloadLanguage = true; public function onDisplay($name) { $js = "
		function jSelectArticle(id, title, catid, object, link, lang)
		{
			var hreflang = '';
			if (lang !== '')
			{
				var hreflang = ' hreflang = \"' + lang + '\"';
			}
			var tag = '<a' + hreflang + ' href=\"' + link + '\">' + title + '</a>';
			jInsertEditorText(tag, '" . $name . "');
			SqueezeBox.close();
		}"; $doc = JFactory::getDocument(); $doc->addScriptDeclaration($js); JHtml::_('behavior.modal'); $link = 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;' . JSession::getFormToken() . '=1'; $button = new JObject; $button->modal = true; $button->class = 'btn'; $button->link = $link; $button->text = JText::_('PLG_ARTICLE_BUTTON_ARTICLE'); $button->name = 'file-add'; $button->options = "{handler: 'iframe', size: {x: 800, y: 500}}"; return $button; } } 