<?php
 defined('_JEXEC') or die; class PlgButtonReadmore extends JPlugin { protected $autoloadLanguage = true; public function onDisplay($name) { $doc = JFactory::getDocument(); $getContent = $this->_subject->getContent($name); $present = JText::_('PLG_READMORE_ALREADY_EXISTS', true); $js = "
			function insertReadmore(editor)
			{
				var content = $getContent
				if (content.match(/<hr\s+id=(\"|')system-readmore(\"|')\s*\/*>/i))
				{
					alert('$present');
					return false;
				} else {
					jInsertEditorText('<hr id=\"system-readmore\" />', editor);
				}
			}
			"; $doc->addScriptDeclaration($js); $button = new JObject; $button->modal = false; $button->class = 'btn'; $button->onclick = 'insertReadmore(\'' . $name . '\');return false;'; $button->text = JText::_('PLG_READMORE_BUTTON_READMORE'); $button->name = 'arrow-down'; $button->link = '#'; return $button; } } 