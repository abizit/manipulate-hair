<?php
 defined('_JEXEC') or die; class PlgSystemLanguagecode extends JPlugin { public function onAfterRender() { $app = JFactory::getApplication(); if ($app->isSite()) { $body = $app->getBody(); $code = JFactory::getDocument()->getLanguage(); $new_code = $this->params->get($code); if ($new_code) { $patterns = array( chr(1) . '(<html.*\s+xml:lang=")(' . $code . ')(".*>)' . chr(1) . 'i', chr(1) . '(<html.*\s+lang=")(' . $code . ')(".*>)' . chr(1) . 'i', ); $replace = array( '${1}' . strtolower($new_code) . '${3}', '${1}' . strtolower($new_code) . '${3}' ); } else { $patterns = array(); $replace = array(); } preg_match_all(chr(1) . '(<link.*\s+hreflang=")([0-9a-z\-]*)(".*\s+rel="alternate".*/>)' . chr(1) . 'i', $body, $matches); foreach ($matches[2] as $match) { $new_code = $this->params->get(strtolower($match)); if ($new_code) { $patterns[] = chr(1) . '(<link.*\s+hreflang=")(' . $match . ')(".*\s+rel="alternate".*/>)' . chr(1) . 'i'; $replace[] = '${1}' . $new_code . '${3}'; } } preg_match_all(chr(1) . '(<link.*\s+rel="alternate".*\s+hreflang=")([0-9A-Za-z\-]*)(".*/>)' . chr(1) . 'i', $body, $matches); foreach ($matches[2] as $match) { $new_code = $this->params->get(strtolower($match)); if ($new_code) { $patterns[] = chr(1) . '(<link.*\s+rel="alternate".*\s+hreflang=")(' . $match . ')(".*/>)' . chr(1) . 'i'; $replace[] = '${1}' . $new_code . '${3}'; } } $app->setBody(preg_replace($patterns, $replace, $body)); } } public function onContentPrepareForm($form, $data) { if (!($form instanceof JForm)) { $this->_subject->setError('JERROR_NOT_A_FORM'); return false; } if ($form->getName() != 'com_plugins.plugin' || !$form->getField('languagecodeplugin', 'params')) { return true; } if ($languages = JLanguage::getKnownLanguages(JPATH_SITE)) { foreach ($languages as $tag => $language) { $form->load('
					<form>
						<fields name="params">
							<fieldset
								name="languagecode"
								label="PLG_SYSTEM_LANGUAGECODE_FIELDSET_LABEL"
								description="PLG_SYSTEM_LANGUAGECODE_FIELDSET_DESC"
							>
								<field
									name="' . strtolower($tag) . '"
									type="text"
									description="' . htmlspecialchars(JText::sprintf('PLG_SYSTEM_LANGUAGECODE_FIELD_DESC', $language['name']), ENT_COMPAT, 'UTF-8') . '"
									translate_description="false"
									label="' . $tag . '"
									translate_label="false"
									size="7"
									filter="cmd"
								/>
							</fieldset>
						</fields>
					</form>
				'); } } return true; } } 