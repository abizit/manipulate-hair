<?php
 defined('_JEXEC') or die; class PlgEditorNone extends JPlugin { public function onInit() { $txt = "<script type=\"text/javascript\">
					function insertAtCursor(myField, myValue)
					{
						if (document.selection)
						{
							// IE support
							myField.focus();
							sel = document.selection.createRange();
							sel.text = myValue;
						} else if (myField.selectionStart || myField.selectionStart == '0')
						{
							// MOZILLA/NETSCAPE support
							var startPos = myField.selectionStart;
							var endPos = myField.selectionEnd;
							myField.value = myField.value.substring(0, startPos)
								+ myValue
								+ myField.value.substring(endPos, myField.value.length);
						} else {
							myField.value += myValue;
						}
					}
				</script>"; return $txt; } public function onSave() { return; } public function onGetContent($id) { return "document.getElementById('$id').value;\n"; } public function onSetContent($id, $html) { return "document.getElementById('$id').value = $html;\n"; } public function onGetInsertMethod($id) { static $done = false; if (!$done) { $doc = JFactory::getDocument(); $js = "\tfunction jInsertEditorText(text, editor)
			{
				insertAtCursor(document.getElementById(editor), text);
			}"; $doc->addScriptDeclaration($js); } return true; } public function onDisplay($name, $content, $width, $height, $col, $row, $buttons = true, $id = null, $asset = null, $author = null, $params = array()) { if (empty($id)) { $id = $name; } if (is_numeric($width)) { $width .= 'px'; } if (is_numeric($height)) { $height .= 'px'; } $buttons = $this->_displayButtons($id, $buttons, $asset, $author); $editor = "<textarea name=\"$name\" id=\"$id\" cols=\"$col\" rows=\"$row\" style=\"width: $width; height: $height;\">$content</textarea>" . $buttons; return $editor; } public function _displayButtons($name, $buttons, $asset, $author) { $return = ''; $args = array( 'name' => $name, 'event' => 'onGetInsertMethod' ); $results = (array) $this->update($args); if ($results) { foreach ($results as $result) { if (is_string($result) && trim($result)) { $return .= $result; } } } if (is_array($buttons) || (is_bool($buttons) && $buttons)) { $buttons = $this->_subject->getButtons($name, $buttons, $asset, $author); $return .= JLayoutHelper::render('joomla.editors.buttons', $buttons); } return $return; } } 