<?php
 defined('_JEXEC') or die; $data = $displayData; ?>
<textarea
	name="<?php echo $data->name; ?>"
	id="<?php echo $data->id; ?>"
	cols="<?php echo $data->cols; ?>"
	rows="<?php echo $data->rows; ?>"
	style="width: <?php echo $data->width; ?>; height: <?php echo $data->height; ?>;"
	class="mce_editable"
>
	<?php echo $data->content; ?>
</textarea>