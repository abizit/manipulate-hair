<?php
 defined('_JEXEC') or die; $doTask = $displayData['doTask']; $class = $displayData['class']; $text = $displayData['text']; $btnClass = $displayData['btnClass']; ?>
<button onclick="<?php echo $doTask; ?>" class="<?php echo $btnClass; ?>">
	<span class="<?php echo trim($class); ?>"></span>
	<?php echo $text; ?>
</button>
