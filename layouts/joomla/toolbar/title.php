<?php
 defined('_JEXEC') or die; $icon = empty($displayData['icon']) ? 'generic' : preg_replace('#\.[^ .]*$#', '', $displayData['icon']); ?>
<h1 class="page-title">
	<span class="icon-<?php echo $icon; ?>"></span>
	<?php echo $displayData['title']; ?>
</h1>
