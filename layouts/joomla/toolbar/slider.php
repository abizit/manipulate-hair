<?php
 defined('_JEXEC') or die; $doTask = $displayData['doTask']; $class = $displayData['class']; $text = $displayData['text']; $name = $displayData['name']; $onClose = $displayData['onClose']; ?>
<button onclick="<?php echo $doTask; ?>" class="btn btn-small" data-toggle="collapse" data-target="#collapse-<?php echo $name; ?>"<?php echo $onClose; ?>>
	<i class="icon-cog"></i>
	<?php echo $text; ?>
</button>
