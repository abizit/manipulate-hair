<?php
 defined('_JEXEC') or die; $title = $displayData['title']; ?>
<button data-toggle="modal" data-target="#collapseModal" class="btn btn-small">
	<i class="icon-checkbox-partial" title="<?php echo $title; ?>"></i>
	<?php echo $title; ?>
</button>
