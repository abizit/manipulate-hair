<?php
 defined('_JEXEC') or die; $form = $displayData->getForm(); $fieldSets = $form->getFieldsets('metadata'); ?>

<?php foreach ($fieldSets as $name => $fieldSet) : ?>
	<?php if (isset($fieldSet->description) && trim($fieldSet->description)) : ?>
		<p class="alert alert-info"><?php echo $this->escape(JText::_($fieldSet->description)); ?></p>
	<?php endif; ?>

	<?php
 if ($name == 'jmetadata') { echo $form->renderField('metadesc'); echo $form->renderField('metakey'); echo $form->renderField('xreference'); } foreach ($form->getFieldset($name) as $field) { if ($field->name != 'jform[metadata][tags][]') { echo $field->renderField(); } } ?>
<?php endforeach; ?>
