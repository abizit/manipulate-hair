<?php
 defined('_JEXEC') or die; $id = empty($displayData['id']) ? '' : (' id="' . $displayData['id'] . '"'); $target = empty($displayData['target']) ? '' : (' target="' . $displayData['target'] . '"'); $onclick = empty($displayData['onclick']) ? '' : (' onclick="' . $displayData['onclick'] . '"'); $title = empty($displayData['title']) ? '' : (' title="' . $this->escape($displayData['title']) . '"'); $text = empty($displayData['text']) ? '' : ('<span>' . $displayData['text'] . '</span>') ?>
<div class="row-fluid"<?php echo $id; ?>>
	<div class="span12">
		<a href="<?php echo $displayData['link']; ?>"<?php echo $target . $onclick . $title; ?>>
			<i class="icon-<?php echo $displayData['image']; ?>"></i> <?php echo $text; ?>
		</a>
	</div>
</div>
