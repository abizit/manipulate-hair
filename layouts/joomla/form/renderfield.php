<?php
 defined('_JEXEC') or die; ?>

<?php
if (!empty($displayData['options']['showonEnabled'])) { JHtml::_('jquery.framework'); JHtml::_('script', 'jui/cms.js', false, true); } ?>

<div class="control-group <?php echo $displayData['options']['class']; ?>" <?php echo $displayData['options']['rel']; ?>>
	<?php if (empty($displayData['options']['hiddenLabel'])) : ?>
		<div class="control-label"><?php echo $displayData['label']; ?></div>
	<?php endif; ?>
	<div class="controls"><?php echo $displayData['input']; ?></div>
</div>
