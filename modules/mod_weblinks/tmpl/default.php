<?php
 defined('_JEXEC') or die; ?>
<ul class="weblinks<?php echo $moduleclass_sfx; ?>">
	<?php
 foreach ($list as $item) : ?>
		<li>
			<?php
 $link = $item->link; switch ($params->get('target', 3)) { case 1: echo '<a href="' . $link . '" target="_blank" rel="' . $params->get('follow', 'nofollow') . '">' . htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8') . '</a>'; break; case 2: echo "<a href=\"#\" onclick=\"window.open('" . $link . "', '', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=550'); return false\">" . htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8') . '</a>'; break; default: echo '<a href="' . $link . '" rel="' . $params->get('follow', 'nofollow') . '">' . htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8') . '</a>'; break; } if ($params->get('description', 0)) { echo nl2br($item->description); } if ($params->get('hits', 0)) { echo '(' . $item->hits . ' ' . JText::_('MOD_WEBLINKS_HITS') . ')'; } ?>
		</li>
	<?php
 endforeach; ?>
</ul>
