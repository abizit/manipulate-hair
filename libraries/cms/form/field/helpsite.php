<?php
 defined('JPATH_PLATFORM') or die; JFormHelper::loadFieldClass('list'); class JFormFieldHelpsite extends JFormFieldList { public $type = 'Helpsite'; protected function getOptions() { $options = array_merge(parent::getOptions(), JHelp::createSiteList(JPATH_ADMINISTRATOR . '/help/helpsites.xml', $this->value)); return $options; } protected function getInput() { JHtml::script('system/helpsite.js', false, true); JFactory::getDocument()->addScriptDeclaration( 'var helpsite_base = "' . addslashes(JUri::root()) . '";' ); $html = parent::getInput(); $button = '<button
						type="button"
						class="btn btn-small"
						id="helpsite-refresh"
						rel="' . $this->id . '"
					>
					<span>' . JText::_('JGLOBAL_HELPREFRESH_BUTTON') . '</span>
					</button>'; return $html . $button; } } 