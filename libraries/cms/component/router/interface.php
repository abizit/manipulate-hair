<?php
 defined('JPATH_PLATFORM') or die; interface JComponentRouterInterface { public function preprocess($query); public function build(&$query); public function parse(&$segments); } 