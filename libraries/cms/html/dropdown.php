<?php
 defined('JPATH_PLATFORM') or die; abstract class JHtmlDropdown { protected static $loaded = array(); protected static $dropDownList = null; public static function init() { if (isset(static::$loaded[__METHOD__])) { return; } JHtml::_('bootstrap.framework'); JFactory::getDocument()->addScriptDeclaration("
			(function($){
				$(document).ready(function (){
					$('.has-context')
					.mouseenter(function (){
						$('.btn-group',$(this)).show();
					})
					.mouseleave(function (){
						$('.btn-group',$(this)).hide();
						$('.btn-group',$(this)).removeClass('open');
					});

					contextAction =function (cbId, task)
					{
						$('input[name=\"cid[]\"]').removeAttr('checked');
						$('#' + cbId).attr('checked','checked');
						Joomla.submitbutton(task);
					}
				});
			})(jQuery);
			" ); static::$loaded[__METHOD__] = true; return; } public static function start() { if (isset(static::$loaded[__METHOD__]) && static::$loaded[__METHOD__] == true) { return; } $dropDownList = '<div class="btn-group" style="margin-left:6px;display:none">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle btn btn-mini">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">'; static::$dropDownList = $dropDownList; static::$loaded[__METHOD__] = true; return; } public static function render() { $dropDownList = static::$dropDownList; $dropDownList .= '</ul></div>'; static::$dropDownList = null; static::$loaded['JHtmlDropdown::start'] = false; return $dropDownList; } public static function edit($id, $prefix = '', $customLink = '') { static::start(); if (!$customLink) { $option = JFactory::getApplication()->input->getCmd('option'); $link = 'index.php?option=' . $option; } else { $link = $customLink; } $link .= '&task=' . $prefix . 'edit&id=' . $id; $link = JRoute::_($link); static::addCustomItem(JText::_('JACTION_EDIT'), $link); return; } public static function publish($checkboxId, $prefix = '') { $task = $prefix . 'publish'; static::addCustomItem(JText::_('JTOOLBAR_PUBLISH'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function unpublish($checkboxId, $prefix = '') { $task = $prefix . 'unpublish'; static::addCustomItem(JText::_('JTOOLBAR_UNPUBLISH'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function featured($checkboxId, $prefix = '') { $task = $prefix . 'featured'; static::addCustomItem(JText::_('JFEATURED'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function unfeatured($checkboxId, $prefix = '') { $task = $prefix . 'unfeatured'; static::addCustomItem(JText::_('JUNFEATURED'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function archive($checkboxId, $prefix = '') { $task = $prefix . 'archive'; static::addCustomItem(JText::_('JTOOLBAR_ARCHIVE'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function unarchive($checkboxId, $prefix = '') { $task = $prefix . 'unpublish'; static::addCustomItem(JText::_('JTOOLBAR_UNARCHIVE'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function trash($checkboxId, $prefix = '') { $task = $prefix . 'trash'; static::addCustomItem(JText::_('JTOOLBAR_TRASH'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function untrash($checkboxId, $prefix = '') { $task = $prefix . 'publish'; static::addCustomItem(JText::_('JTOOLBAR_UNTRASH'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function checkin($checkboxId, $prefix = '') { $task = $prefix . 'checkin'; static::addCustomItem(JText::_('JTOOLBAR_CHECKIN'), 'javascript:void(0)', 'onclick="contextAction(\'' . $checkboxId . '\', \'' . $task . '\')"'); return; } public static function divider() { static::$dropDownList .= '<li class="divider"></li>'; return; } public static function addCustomItem($label, $link = 'javascript:void(0)', $linkAttributes = '', $className = '', $ajaxLoad = false, $jsCallBackFunc = null) { static::start(); if ($ajaxLoad) { $href = ' href = "javascript:void(0)" onclick="loadAjax(\'' . $link . '\', \'' . $jsCallBackFunc . '\')"'; } else { $href = ' href = "' . $link . '" '; } $dropDownList = static::$dropDownList; $dropDownList .= '<li class="' . $className . '"><a ' . $linkAttributes . $href . ' >'; $dropDownList .= $label; $dropDownList .= '</a></li>'; static::$dropDownList = $dropDownList; return; } } 