<?php
 defined('JPATH_PLATFORM') or die; abstract class JHtmlFormbehavior { protected static $loaded = array(); public static function chosen($selector = '.advancedSelect', $debug = null, $options = array()) { if (isset(static::$loaded[__METHOD__][$selector])) { return; } JHtml::_('jquery.framework'); if ($debug === null) { $config = JFactory::getConfig(); $debug = (boolean) $config->get('debug'); } $options['disable_search_threshold'] = isset($options['disable_search_threshold']) ? $options['disable_search_threshold'] : 10; $options['allow_single_deselect'] = isset($options['allow_single_deselect']) ? $options['allow_single_deselect'] : true; $options['placeholder_text_multiple'] = isset($options['placeholder_text_multiple']) ? $options['placeholder_text_multiple']: JText::_('JGLOBAL_SELECT_SOME_OPTIONS'); $options['placeholder_text_single'] = isset($options['placeholder_text_single']) ? $options['placeholder_text_single'] : JText::_('JGLOBAL_SELECT_AN_OPTION'); $options['no_results_text'] = isset($options['no_results_text']) ? $options['no_results_text'] : JText::_('JGLOBAL_SELECT_NO_RESULTS_MATCH'); $options_str = json_encode($options, ($debug && defined('JSON_PRETTY_PRINT') ? JSON_PRETTY_PRINT : false)); JHtml::_('script', 'jui/chosen.jquery.min.js', false, true, false, false, $debug); JHtml::_('stylesheet', 'jui/chosen.css', false, true); JFactory::getDocument()->addScriptDeclaration("
				jQuery(document).ready(function (){
					jQuery('" . $selector . "').chosen(" . $options_str . ");
				});
			" ); static::$loaded[__METHOD__][$selector] = true; return; } public static function ajaxchosen(JRegistry $options, $debug = null) { $selector = $options->get('selector', '.tagfield'); $type = $options->get('type', 'GET'); $url = $options->get('url', null); $dataType = $options->get('dataType', 'json'); $jsonTermKey = $options->get('jsonTermKey', 'term'); $afterTypeDelay = $options->get('afterTypeDelay', '500'); $minTermLength = $options->get('minTermLength', '3'); JText::script('JGLOBAL_KEEP_TYPING'); JText::script('JGLOBAL_LOOKING_FOR'); if (!empty($url)) { if (isset(static::$loaded[__METHOD__][$selector])) { return; } JHtml::_('jquery.framework'); static::chosen($selector, $debug); JHtml::_('script', 'jui/ajax-chosen.min.js', false, true, false, false, $debug); JFactory::getDocument()->addScriptDeclaration("
				(function($){
					$(document).ready(function () {
						$('" . $selector . "').ajaxChosen({
							type: '" . $type . "',
							url: '" . $url . "',
							dataType: '" . $dataType . "',
							jsonTermKey: '" . $jsonTermKey . "',
							afterTypeDelay: '" . $afterTypeDelay . "',
							minTermLength: '" . $minTermLength . "'
						}, function (data) {
							var results = [];

							$.each(data, function (i, val) {
								results.push({ value: val.value, text: val.text });
							});

							return results;
						});
					});
				})(jQuery);
				" ); static::$loaded[__METHOD__][$selector] = true; } return; } } 