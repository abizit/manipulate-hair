<?php
 defined('JPATH_PLATFORM') or die; abstract class JHtmlSortablelist { protected static $loaded = array(); public static function sortable($tableId, $formId, $sortDir = 'asc', $saveOrderingUrl, $proceedSaveOrderButton = true, $nestedList = false) { if (isset(static::$loaded[__METHOD__])) { return; } JHtml::_('jquery.ui', array('core', 'sortable')); JHtml::_('script', 'jui/sortablelist.js', false, true); JHtml::_('stylesheet', 'jui/sortablelist.css', false, true, false); JFactory::getDocument()->addScriptDeclaration("
			(function ($){
				$(document).ready(function (){
					var sortableList = new $.JSortableList('#" . $tableId . " tbody','" . $formId . "','" . $sortDir . "' , '" . $saveOrderingUrl . "','','" . $nestedList . "');
				});
			})(jQuery);
			" ); if ($proceedSaveOrderButton) { static::_proceedSaveOrderButton(); } static::$loaded[__METHOD__] = true; return; } public static function _proceedSaveOrderButton() { JFactory::getDocument()->addScriptDeclaration( "(function ($){
				$(document).ready(function (){
					var saveOrderButton = $('.saveorder');
					saveOrderButton.css({'opacity':'0.2', 'cursor':'default'}).attr('onclick','return false;');
					var oldOrderingValue = '';
					$('.text-area-order').focus(function ()
					{
						oldOrderingValue = $(this).attr('value');
					})
					.keyup(function (){
						var newOrderingValue = $(this).attr('value');
						if (oldOrderingValue != newOrderingValue)
						{
							saveOrderButton.css({'opacity':'1', 'cursor':'pointer'}).removeAttr('onclick')
						}
					});
				});
			})(jQuery);" ); return; } } 