<?php
 defined('FOF_INCLUDED') or die; class FOFModelFieldBoolean extends FOFModelFieldNumber { public function isEmpty($value) { return is_null($value) || ($value === ''); } } 