<?php
 defined('FOF_INCLUDED') or die(); interface FOFConfigDomainInterface { public function parseDomain(SimpleXMLElement $xml, array &$ret); public function get(&$configuration, $var, $default); } 