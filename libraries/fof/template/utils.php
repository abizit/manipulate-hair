<?php
 defined('FOF_INCLUDED') or die; class FOFTemplateUtils { public static function addCSS($path) { $document = FOFPlatform::getInstance()->getDocument(); if ($document instanceof JDocument) { if (method_exists($document, 'addStyleSheet')) { $url = self::parsePath($path); $document->addStyleSheet($url); } } } public static function addJS($path, $defer = false, $async = false) { $document = FOFPlatform::getInstance()->getDocument(); if ($document instanceof JDocument) { if (method_exists($document, 'addScript')) { $url = self::parsePath($path); $document->addScript($url, "text/javascript", $defer, $async); } } } public static function addLESS($path, $altPath = null, $returnPath = false) { static $sanityCheck = null; $localFile = self::parsePath($path, true); $filesystem = FOFPlatform::getInstance()->getIntegrationObject('filesystem'); $platformDirs = FOFPlatform::getInstance()->getPlatformBaseDirs(); if (is_null($sanityCheck)) { if (!is_dir($platformDirs['public'] . '/media/lib_fof/compiled/')) { $sanityCheck = $filesystem->folderCreate($platformDirs['public'] . '/media/lib_fof/compiled/'); } else { $sanityCheck = true; } } if (!$sanityCheck || !is_file($localFile)) { if (!$returnPath) { if (is_string($altPath)) { self::addCSS($altPath); } elseif (is_array($altPath)) { foreach ($altPath as $anAltPath) { self::addCSS($anAltPath); } } } return false; } $id = md5(filemtime($localFile) . filectime($localFile) . $localFile); $cachedPath = $platformDirs['public'] . '/media/lib_fof/compiled/' . $id . '.css'; $lessCompiler = new FOFLess; $lessCompiler->formatterName = 'compressed'; $altFiles = self::getAltPaths($path); if (isset($altFiles['alternate'])) { $currentLocation = realpath(dirname($localFile)); $normalLocation = realpath(dirname($altFiles['normal'])); $alternateLocation = realpath(dirname($altFiles['alternate'])); if ($currentLocation == $normalLocation) { $lessCompiler->importDir = array($alternateLocation, $currentLocation); } else { $lessCompiler->importDir = array($currentLocation, $normalLocation); } } $lessCompiler->checkedCompile($localFile, $cachedPath); $base_url = rtrim(FOFPlatform::getInstance()->URIbase(), '/'); if (substr($base_url, -14) == '/administrator') { $base_url = substr($base_url, 0, -14); } $url = $base_url . '/media/lib_fof/compiled/' . $id . '.css'; if ($returnPath) { return $url; } else { $document = FOFPlatform::getInstance()->getDocument(); if ($document instanceof JDocument) { if (method_exists($document, 'addStyleSheet')) { $document->addStyleSheet($url); } } return true; } } public static function sefSort($text, $field, $list) { $sort = JHTML::_('grid.sort', JText::_(strtoupper($text)) . '&nbsp;', $field, $list->order_Dir, $list->order); return str_replace('href="#"', 'href="javascript:void(0);"', $sort); } public static function parsePath($path, $localFile = false) { $platformDirs = FOFPlatform::getInstance()->getPlatformBaseDirs(); if ($localFile) { $url = rtrim($platformDirs['root'], DIRECTORY_SEPARATOR) . '/'; } else { $url = FOFPlatform::getInstance()->URIroot(); } $altPaths = self::getAltPaths($path); $filePath = $altPaths['normal']; if (defined('JDEBUG') && JDEBUG && isset($altPaths['debug'])) { if (file_exists($platformDirs['public'] . '/' . $altPaths['debug'])) { $filePath = $altPaths['debug']; } } elseif (isset($altPaths['alternate'])) { if (file_exists($platformDirs['public'] . '/' . $altPaths['alternate'])) { $filePath = $altPaths['alternate']; } } $url .= $filePath; return $url; } public static function getAltPaths($path) { $protoAndPath = explode('://', $path, 2); if (count($protoAndPath) < 2) { $protocol = 'media'; } else { $protocol = $protoAndPath[0]; $path = $protoAndPath[1]; } $path = ltrim($path, '/' . DIRECTORY_SEPARATOR); switch ($protocol) { case 'media': $pathAndParams = explode('?', $path, 2); $ret = array( 'normal' => 'media/' . $pathAndParams[0], 'alternate' => FOFPlatform::getInstance()->getTemplateOverridePath('media:/' . $pathAndParams[0], false), ); break; case 'admin': $ret = array( 'normal' => 'administrator/' . $path ); break; default: case 'site': $ret = array( 'normal' => $path ); break; } $filesystem = FOFPlatform::getInstance()->getIntegrationObject('filesystem'); $ext = $filesystem->getExt($ret['normal']); if (in_array($ext, array('css', 'js'))) { $file = basename($filesystem->stripExt($ret['normal'])); if (strlen($file) > 4 && strrpos($file, '.min', '-4')) { $position = strrpos($file, '.min', '-4'); $filename = str_replace('.min', '.', $file, $position); } else { $filename = $file . '-uncompressed.' . $ext; } $t1 = (object) $ret; $temp = clone $t1; unset($t1); $temp = (array)$temp; $normalPath = explode('/', $temp['normal']); array_pop($normalPath); $normalPath[] = $filename; $ret['debug'] = implode('/', $normalPath); } return $ret; } public static function loadPosition($position, $style = -2) { $document = FOFPlatform::getInstance()->getDocument(); if (!($document instanceof JDocument)) { return ''; } if (!method_exists($document, 'loadRenderer')) { return ''; } try { $renderer = $document->loadRenderer('module'); } catch (Exception $exc) { return ''; } $params = array('style' => $style); $contents = ''; foreach (JModuleHelper::getModules($position) as $mod) { $contents .= $renderer->render($mod, $params); } return $contents; } public static function route($route = '') { $route = trim($route); if ($route == 'index.php' || $route == 'index.php?') { $result = $route; } elseif (substr($route, 0, 1) == '&') { $url = JURI::getInstance(); $vars = array(); parse_str($route, $vars); $url->setQuery(array_merge($url->getQuery(true), $vars)); $result = 'index.php?' . $url->getQuery(); } else { $url = JURI::getInstance(); $props = $url->getQuery(true); if (substr($route, 0, 10) == 'index.php?') { $route = substr($route, 10); } $parts = array(); parse_str($route, $parts); $result = array(); if (!isset($parts['option']) && isset($props['option'])) { $result[] = 'option=' . $props['option']; } if (!isset($parts['view']) && isset($props['view'])) { $result[] = 'view=' . $props['view']; if (!isset($parts['layout']) && isset($props['layout'])) { $result[] = 'layout=' . $props['layout']; } } if (!isset($parts['format']) && isset($props['format']) && $props['format'] != 'html') { $result[] = 'format=' . $props['format']; } if (!empty($route)) { $result[] = $route; } $result = 'index.php?' . implode('&', $result); } return JRoute::_($result); } } 