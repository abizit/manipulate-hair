<?php
 namespace Joomla\DI; interface ContainerAwareInterface { public function getContainer(); public function setContainer(Container $container); } 