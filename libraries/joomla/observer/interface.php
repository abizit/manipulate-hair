<?php
 defined('JPATH_PLATFORM') or die; interface JObserverInterface { public static function createObserver(JObservableInterface $observableObject, $params = array()); } 