<?php
 defined('JPATH_PLATFORM') or die; interface JObservableInterface { public function attachObserver(JObserverInterface $observer); } 