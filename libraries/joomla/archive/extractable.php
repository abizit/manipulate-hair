<?php
 defined('JPATH_PLATFORM') or die; interface JArchiveExtractable { public function extract($archive, $destination, array $options = array()); public static function isSupported(); } 