<?php
 defined('JPATH_PLATFORM') or die(); class JLinkedinCommunications extends JLinkedinObject { public function inviteByEmail($email, $first_name, $last_name, $subject, $body, $connection = 'friend') { $token = $this->oauth->getToken(); $parameters = array( 'oauth_token' => $token['key'] ); $this->oauth->setOption('success_code', 201); $base = '/v1/people/~/mailbox'; $xml = '<mailbox-item>
				  <recipients>
				  	<recipient>
						<person path="/people/email=' . $email . '">
							<first-name>' . $first_name . '</first-name>
							<last-name>' . $last_name . '</last-name>
						</person>
					</recipient>
				</recipients>
				<subject>' . $subject . '</subject>
				<body>' . $body . '</body>
				<item-content>
				    <invitation-request>
				      <connect-type>' . $connection . '</connect-type>
				    </invitation-request>
				</item-content>
			 </mailbox-item>'; $header['Content-Type'] = 'text/xml'; $path = $this->getOption('api.url') . $base; $response = $this->oauth->oauthRequest($path, 'POST', $parameters, $xml, $header); return $response; } public function inviteById($id, $first_name, $last_name, $subject, $body, $connection = 'friend') { $token = $this->oauth->getToken(); $parameters = array( 'oauth_token' => $token['key'] ); $base = '/v1/people-search:(people:(api-standard-profile-request))'; $data['format'] = 'json'; $data['first-name'] = $first_name; $data['last-name'] = $last_name; $path = $this->getOption('api.url') . $base; $response = $this->oauth->oauthRequest($path, 'GET', $parameters, $data); if (strpos($response->body, 'apiStandardProfileRequest') === false) { throw new RuntimeException($response->body); } $value = explode('"value": "', $response->body); $value = explode('"', $value[1]); $value = $value[0]; $value = explode(':', $value); $name = $value[0]; $value = $value[1]; $this->oauth->setOption('success_code', 201); $base = '/v1/people/~/mailbox'; $xml = '<mailbox-item>
				  <recipients>
				  	<recipient>
						<person path="/people/id=' . $id . '">
						</person>
					</recipient>
				</recipients>
				<subject>' . $subject . '</subject>
				<body>' . $body . '</body>
				<item-content>
				    <invitation-request>
				      <connect-type>' . $connection . '</connect-type>
				      <authorization>
				      	<name>' . $name . '</name>
				        <value>' . $value . '</value>
				      </authorization>
				    </invitation-request>
				</item-content>
			 </mailbox-item>'; $header['Content-Type'] = 'text/xml'; $path = $this->getOption('api.url') . $base; $response = $this->oauth->oauthRequest($path, 'POST', $parameters, $xml, $header); return $response; } public function sendMessage($recipient, $subject, $body) { $token = $this->oauth->getToken(); $parameters = array( 'oauth_token' => $token['key'] ); $this->oauth->setOption('success_code', 201); $base = '/v1/people/~/mailbox'; $xml = '<mailbox-item>
				  <recipients>'; if (is_array($recipient)) { foreach ($recipient as $r) { $xml .= '<recipient>
							<person path="/people/' . $r . '"/>
						</recipient>'; } } $xml .= '</recipients>
				 <subject>' . $subject . '</subject>
				 <body>' . $body . '</body>
				</mailbox-item>'; $header['Content-Type'] = 'text/xml'; $path = $this->getOption('api.url') . $base; $response = $this->oauth->oauthRequest($path, 'POST', $parameters, $xml, $header); return $response; } } 