<?php
 defined('JPATH_PLATFORM') or die; class JFormRuleBoolean extends JFormRule { protected $regex = '^(?:[01]|true|false)$'; protected $modifiers = 'i'; } 