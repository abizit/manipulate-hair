<?php
 defined('JPATH_PLATFORM') or die; interface JController extends Serializable { public function execute(); public function getApplication(); public function getInput(); } 